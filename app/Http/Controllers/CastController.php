<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CastController extends Controller
{
    public function create(){
        return view('cast.create');
    }

    public function store(Request $request){
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        DB::table('cast')->insert(
            [
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
            ]
        );
        return redirect('/cast/create');
    }

    public function index(){
        $cast = DB::table('cast')->get();

        return view('cast.index', compact('cast'));
    }
}
