<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function bio(){
        return view('page.form');
    }

    public function kirim(Request $request){
        $nama = $request['nama'];
        $kota = $request['kota'];
        return view('page.welcome', compact('nama','kota'));
    }
}
