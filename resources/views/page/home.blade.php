@extends('layout.master')

@section('judul')
    <h3>Cara Bergabung ke Media Online</h3>
@endsection

@section('content')
    <ol>
        <li>Mengunjungi Website ini</li>
        <li>Mendaftarkan di <a href="/form">Form Sign Up</a></li>
        <li>Selesai</li>
    </ol>
@endsection